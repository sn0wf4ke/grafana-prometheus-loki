# Grafana Loki Promtail Prometheus

this is a prototype to deploy a monitoring stack consisting on

* `grafana` to visualize data
* `prometheus` to scrape metrics endpoints
* `loki` to aggregate and centralize logs

* `node_exporter` to expose metrics to `prometheus`
* `promtail` to collect, nomalize and push logs to `loki`

it is docker based so docker and traefik will be installed

## Usage

* clone recursively

```pwsh
git clone --recursive https://gitlab.com/sn0wf4ke/grafana-prometheus-loki.git
```

* adjust `pip.conf`, `prepare.sh` and `inventory/group_vars/monitoring/global.yml` to your needs (for aptcacher, pip-mirror and docker private registry)

* set environment

```pwsh
$env:BRIDGE="eth0" # for virtualbox
```

* start vagrant box with

```pwsh
vagrant up
```

* deploy monitoring stack with:

```
ansible-playbook playbook.yml
```

## Accessability

* you can access traefik dashboard on `http://<vagrant machine ip>:8080/dashboard/` (slash on end is mandatory, `test:test`)
* you can access grafana on `https://grafana` (after setting your hosts file `admin:admin`)
* you can access prometheus on `https://prometheus` (`admin:prometheus`)
* you can't access loki on `http://loki` cause you do not have a valid client cert

## Development

* run tests with `ansible-playbook tests.yml`

## TODO

* [x] add traefik to prepare ssl termination
* [x] add ssl certificates for promtail client auth
* [x] implement promtail client auth via certificates
* [x] implement prometheus auth and tls encryption
* [x] implement auth and tls encryption for node_exporter
* [ ] enable and harden promtail metrics endpoint
* [ ] review hardened config
