VIRT=$( sudo dmidecode | sed -n 's/Product\sName:\s\([^\s]\)/\1/p' | head -n1 )
INTERFACE="eth0"
if [ $VIRT == "VirtualBox" ]; then
INTERFACE="eth1"
# remove virtualbox default route so ansible_default_ipv4.address is the address of eth1
sudo ip route del default via 10.0.2.2
fi
echo -e "vagrant\nvagrant" | sudo passwd vagrant
export DEBIAN_FRONTEND=noninteractive
sudo echo 'Acquire::http::Proxy "http://192.168.5.251:3142/";' > /etc/apt/apt.conf.d/50proxy.conf
sudo apt-get update
sudo mkdir -p /etc/xdg/pip
sudo cp /vagrant/pip.conf /etc/xdg/pip/pip.conf
